
public class Fraction {
	private int numerateur;
	private int denominateur;

	public Fraction(int num, int den) {
		this.numerateur = num;
		this.denominateur = den;
	}

	public String toString() {

		StringBuilder str = new StringBuilder();
		// str.append(numerateur);
		str.append('/');
		str.append(denominateur);
		return str.toString();
	}
}
